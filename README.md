### What is this LGTscan.py for? ###

* It identifies inter-clade LGT events using Embedded Quartet Analysis
* Version 0.1

### Dependencies ###

* Python libraries:
  
  BioPython
  
  numpy
  
  networkx

* 3rd party algorithms
  
  Prodigal
  
  Nucmer
  
  BLAT
  
  muscle
  
  fasttree
  
  PHYLIP package
  
  puzzle

Note that you should make all the 3rd party algorithms in env path. You can do so by running:

export PATH=$PATH:/path/to/the/binary/

### Usage ###

Basic usage:

    LGTscan.py [options] -c/--conf <config.file> -o/--outdir <output directory>

Detailed options:

    Options:

    --version             show program's version number and exit
    -h, --help            show this help message and exit

  Compulsory parameters:
  
  There options are compulsory, and may be supplied in any order.

    -o DIR, --outdir=DIR
                        The output directory where all results and
                        intermediary results will be stored at.
    -i DIR, --indir=DIR
                        The input directory of all genomes in fasta file named
                        as [genome_name].fna.
    -c FILE, --config=FILE
                        The configuration file of the clades.

  Optional parameters:

    There options are optional, and may be supplied in any order.

    -t INT, --num_proc=INT
                        Number of processor to use [default: 1].

*** config file format ***

using Campylobacter as example:

    clade1	ATCC.49349	P854	1336	2010D_8088	2010D_7880	BIGS0006
    clade2	M1	2010D_7725	2010D_7739	2010D_8036	K1	LMG.23211
    clade3	RM2228	2010D_7937	1948	Z163
    clade4	P110B	2010D_8766	LMG.23218	2010D_8755	2010D_7743