#!/usr/bin/env/python
# -*- coding: utf-8 -*- 

__author__ = 'Chengwei Luo (cluo@broadinstitute.org)'
__version__ = '0.0.1'
__date__ = 'May 2015'

"""
ConStrains: identifying Conspecific Strains with metagenomic reads

Copyright(c) 2013 Chengwei Luo (luo.chengwei@gatech.edu), Georgia Institute of Technology

	This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>

for help, type:
python LGTscan.py --help
"""

USAGE = \
"""Usage: 
%prog [options] -c/--conf <config.file> -o/--outdir <output directory>

Python libraries required:
  BioPython
  numpy
  networkx

3rd party algorithm/software required:
  Prodigal
  Nucmer
  BLAT
  muscle
  fasttree
  PHYLIP
  puzzle
  
"""

import sys, os, re, glob, shutil
from optparse import OptionParser, OptionGroup
import multiprocessing as mp
from subprocess import PIPE, Popen, call
from Bio import SeqIO, AlignIO, Phylo
#from ete2 import Tree
import networkx as nx
from itertools import combinations

################## UTILITIES #################
def which(program):
    def is_exe(fpath):
        return os.path.isfile(fpath) and os.access(fpath, os.X_OK)

    fpath, fname = os.path.split(program)
    if fpath:
        if is_exe(program):
            return program
    else:
        for path in os.environ["PATH"].split(os.pathsep):
            path = path.strip('"')
            exe_file = os.path.join(path, program)
            if is_exe(exe_file):
                return exe_file

    return None
    
def run_prodigal(prodigal, outdir, genome, fna_file):
	ffn_tmp = outdir+'/prodigal_genes_tmp/'+genome+'.ffn.tmp'
	faa_tmp = outdir+'/prodigal_genes_tmp/'+genome+'.faa.tmp'
	gbk_tmp = outdir+'/prodigal_genes_tmp/'+genome+'.gbk.tmp'
	log_tmp = outdir+'/prodigal_genes_tmp/'+genome+'.log'
	call([prodigal, '-a', faa_tmp, '-d', ffn_tmp, '-f', 'gbk', \
		 '-i', fna_file, '-o', gbk_tmp, '-p', 'single'], \
		 stdout = PIPE, stderr = open(log_tmp, 'a'))
	
	
	ffn_file = outdir+'/prodigal_genes/'+genome+'.ffn'
	faa_file = outdir+'/prodigal_genes/'+genome+'.faa'
	
	ffn_fh = open(ffn_file, 'w')
	faa_fh = open(faa_file, 'w')
	for record in SeqIO.parse(ffn_tmp, 'fasta'):
		x = record.description.split(' # ')
		tag = x[0]+'|'+x[1]+'-'+x[2]+'|'+x[3]
		ffn_fh.write('>%s|%s\n%s\n' % (genome, tag, str(record.seq)))
	for record in SeqIO.parse(faa_tmp, 'fasta'):
		x = record.description.split(' # ')
		tag = x[0]+'|'+x[1]+'-'+x[2]+'|'+x[3]
		faa_fh.write('>%s|%s\n%s\n' % (genome, tag, str(record.seq)))
	
	ffh_fh.close()
	faa_fh.close()
	
	return 0

def run_blat(blat, ref_ffn, ffn, outfile):
	call([blat, ref_ffn, ffn, '-out=blast8', outfile], stderr = PIPE, stdout = PIPE)
	return 0

def run_muscle(muscle, infile, outfile):
	call([muscle, '-in', infile, '-out', outfile], stderr = PIPE, stdout = PIPE)
	return 0

def filter_blat(blatfile):
	lib = {}
	old_query = ''
	for line in open(blatfile, 'r'):
		cols = line[:-1].split('\t')
		query, ref = cols[:2]
		if query == old_query: continue
		old_query = query
		qstart_st, qend_st = cols[0].split('|')[2].split('-')
		rstart_st, rend_st = cols[1].split('|')[2].split('-')
		qlen = abs(int(qstart_st) - int(qend_st))+1
		rlen = abs(int(rstart_st) - int(rend_st))+1
		if qlen < 300 or rlen < 300: continue
		align_id = float(cols[2])
		align_len = int(cols[3])
		evalue = float(cols[-2])
		if evalue > 1e-6 or align_id < 70: continue
		align_perc = float(align_len)/rlen
		if align_perc < 0.7: continue
		lib[query] = ref
	return lib

def extract_RBMs(blat1, blat2):
	libA=filter_blat(blat1)
	libB=filter_blat(blat2)
	rbms = []
	for geneA in libA:
		geneB=libA[geneA]
		if geneB in libB and libB[geneB]==geneA:
			rbms.append([geneA, geneB])
	return rbms

def make_gene_aln(muscle, infile, outfile):
	call([muscle, '-in', infile, '-out', outfile], stderr = PIPE, stdout = PIPE)
	return 0		

def make_gene_tree(fasttree, infile, outfile):
	ofh = open(outfile, 'w')
	call([fasttree, '-nt', infile], stdout = ofh, stderr = PIPE)
	ofh.close()
	return 0
	
def getAlpha(puzzleFile):
	pfh=open(puzzleFile,'r')
	alpha=0
	while 1:
		line=pfh.readline()
		if not line:
			break
		if re.match('Gamma distribution parameter',line):
			ele=line.split(' ')
			alpha=float(ele[8])
	pfh.close()
	return alpha

def fileLines(infile):
    p = Popen(['wc', '-l', infile], stdout=PIPE, stderr=PIPE)
    result, err = p.communicate()
    if p.returncode != 0:
        raise IOError(err)
    return int(result.strip().split()[0])	
    
def genAllQuartets(elements, cladeDict):
	if len(elements)<4:
		return []
	preCombinations=list(itertools.combinations(elements, 4))
	Combinations=[]
	for comb in preCombinations:
		clades={}
		for ele in comb:
			cladeNum=cladeDict[ele]
			if cladeNum not in clades:
				clades[cladeNum]=1
			else:
				clades[cladeNum]+=1
		if len(clades.keys())!=2:
			continue
		if clades.values()[0]==2 and clades.values()[1]==2:
			Combinations.append(comb)
	return Combinations
    
    
################## MAIN #################
def main(argv = sys.argv[1:]):

	parser = OptionParser(usage = USAGE, version="Version: " + __version__)

	# Compulsory arguments
	compOptions = OptionGroup(parser, "Compulsory parameters",
						"There options are compulsory, and may be supplied in any order.")
	
	compOptions.add_option("-o", "--outdir", type = "string", metavar = "DIR",
							help = "The output directory where all results and intermediary results will be stored at.")
	
	compOptions.add_option("-i", "--indir", type = "string", metavar = "DIR",
							help = "The input directory of all genomes in fasta file named as [genome_name].fna.")
	
	compOptions.add_option("-c", "--config", type = "string", metavar = "FILE",
							help = "The configuration file of the clades.")
	
	parser.add_option_group(compOptions)
	
	optOptions = OptionGroup(parser, "Optional parameters",
						"There options are optional, and may be supplied in any order.")

	optOptions.add_option("-t", "--num_proc", type = "int", default = 1, metavar = 'INT',
							help = "Number of processor to use [default: 1].")
	
	parser.add_option_group(optOptions)
	
	try:
		options, x = parser.parse_args(argv)
	except:
		parser.error('[FATAL]: parsing failure.\n')
		exit(1)

	# examine input directory
	if options.indir == None:
		parser.error('[FATAL]: You have to specify an input directory.\n')
		exit(1)
	
	genome_files = {}
	for fna in glob.glob(options.indir+'/*.fna'):
		genome_name = os.path.basename(fna).replace('.fna','')
		genome_files[genome_name] = os.path.abspath(fna)
	if len(genome_files) == 0:
		sys.stderr.write('[FATAL]: cannot detect any genome files in input directory, please check your input.\n')
		exit(1)
	
	sys.stdout.write('Input genomes and the corresponding genome FASTA file:\n\n')
	for genome_name in sorted(genome_files.keys()):
		sys.stdout.write('  [%s]\t%s\n' % (genome_name, genome_files[genome_name]))
	sys.stdout.write('*****************************************************\n\n')
		
	if options.outdir == None:
		parser.error('[FATAL]: You have to specify an output directory.\n')
		exit(1)
	if not os.path.exists(options.outdir):
		try: 
			os.mkdir(options.outdir)
		except: 
			sys.stderr.write('[FATAL]: Failed in creating output directory: %s, please make sure you have the previlege.\n' % options.outdir)
			exit(1)
	
	clades = {}
	if options.config == None:
		parser.error('[FATAL]: You have to specify a clade config file.\n')
		exit(1)
	elif os.path.exists(options.config):	
		try:
			for line in open(options.config, 'r'):
				cols = line[:-1].split('\t')
				clades[cols[0]] = cols[1:]
		except:
			sys.stderr.write('[WARNING]: Failure in reading config file, will continue with automated clade identification process.\n')
			clades = {}
	
	if len(clades) == 0:
		sys.stdout.write('No clade configuration supplied, going to initiate clade identification.\n')
	else:
		for clade in clades:
			for genome in clades[clade]:
				try: genome_file = genome_files[genome]
				except KeyError:
					sys.stderr.write('[FATAL]: cannot locate genome FASTA file for %s in clade: %s.\n' % (genome, clade))
					exit(1)
	
	if len(clades) == 0:
		sys.stdout.write('No clade config file supplied, we will determine the clades internally.\n')
	else:
		for x in sorted(clades.keys()):
			sys.stdout.write('[%s] members:\n' % x)
			sys.stdout.write('      %s\n' % ' '.join(clades[x]))
			
	# test third party software
	algos = ['prodigal', 'nucmer', 'blat', 'muscle', 'fasttree', 'puzzle', 'seqboot', 'neighbor']  # specify the 3rd party algos.
	for algo in algos:
		if which(algo) == None:
			sys.stderr.write('[FATAL]: cannot locate %s. Abort!\n' % algo)
			exit(1)
	
	# prep data
	clade_dict = {}
	genomes = []
	genome_index = {}
	for clade in clades:
		for genome in clades[clade]:
			clade_dict[genome] = clade
			genomes.append(genome)
	for i, genome in enumerate(genomes): genome_index[genome] = i
	
	# start processing
	# 1. identify genes
	prodigal_cmds = []
	prodigal_dir = options.outdir + '/prodigal_genes/'
	prodigal_tempdir = options.outdir + '/prodigal_genes_tmp/'
	if not os.path.exists(prodigal_dir): os.mkdir(prodigal_dir)
	for genome in genome_files:
		prodigal_outfile = prodigal_dir + genome + '.ffn'
		if not os.path.exists(prodigal_outfile):
			prodigal_cmds.append([algos[0], options.outdir, genome, genome_files[genome]])
	if len(prodigal_cmds) > 0:
		if not os.path.exists(prodigal_tempdir): os.mkdir(prodigal_tempdir)
		
	pool = mp.Pool(options.num_proc)
	for cmd in prodigal_cmds: pool.apply_async(run_prodigal, args = cmd)
	pool.close()
	pool.join()
	if os.path.exists(prodigal_tempdir): shutil.rmtree(prodigal_tempdir)
	sys.stdout.write('All genomes have genes predicted.\n')
	
	# 2. make core genome and genome tree
	# 2.1 run all BLAT
	blat_cmds = []
	blat_dir = options.outdir+'/blat/'
	if not os.path.exists(blat_dir): os.mkdir(blat_dir)
	for genome1 in genome_files:
		ffn_file1 = options.outdir + '/prodigal_genes/'+genome1+'.ffn'
		for genome2 in genome_files:
			if genome1 == genome2: continue
			ffn_file2 = options.outdir + '/prodigal_genes/'+genome2+'.ffn'
			blat_file = blat_dir+genome1+'.vs.'+genome2+'.blat'
			if not os.path.exists(blat_file):
				blat_cmds.append([algos[2], ffn_file2, ffn_file1, blat_file])
	pool = mp.Pool(options.num_proc)
	for cmd in blat_cmds: pool.apply_async(run_blat, args = cmd)
	pool.close()
	pool.join()
	sys.stdout.write('All BLAT runs are done!\n')
	
	# 2.2 build the pangenome
	pangenome_file = options.outdir + '/pangenome.txt'
	if not os.path.exists(pangenome_file):
		G = nx.Graph()
		edges = []
		for genome1 in genome_files:
			for genome2 in genome_files:
				if genome1 == genome2: continue
				blat_file1 = blat_dir + genome1+'.vs.'+genome2+'.blat'
				blat_file2 = blat_dir + genome2+'.vs.'+genome1+'.blat'
				rbms = extract_RBMs(blat_file1, blat_file2)
				for gene1, gene2 in rbms: edges.append((gene1, gene2))
				G.add_edges_from(edges)
		ofh = open(pangenome_file, 'w')
		ofh.write('#pangeome_id\t%s\n' % ('\t'.join(genomes)))
		for gene_index, component in enumerate(nx.connected_components(G)):
			genes = ['-' for i in range(len(genomes))]
			for gene in component:
				genome = gene.split('|')[0]
				genome_x = genome_index[genome]
				genes[genome_x] = gene
			ofh.write('PAN_%i\t%s\n' % (gene_index+1, '\t'.join(genes)))
		ofh.close()	
	
	# screen pangenome for candidate genes
	candidate_genes = {}
	for line in open(pangenome_file, 'r'):
		if line[0] == '#': continue
		cols = line[:-1].split('\t')
		panID = cols[0]
		gene_clades = {}
		for gene in cols[1:]:
			genome = gene.split('|')[0]
			try:clade = clade_dict[genome]
			except: continue
			if clade not in gene_clades: gene_clades[clade] = []
			gene_clades[clade].append(gene)
		if len(gene_clades) < 2: continue
		sorted_gene_clades = sorted(gene_clades.iteritems(), key = lambda x: len(x[1]), reverse = 1)
		if len(sorted_gene_clades[1][1])<2: continue
		candidate_genes[panID] = gene_clades
	sys.stdout.write('Pangenome generated, %i candidate genes!\n' % len(candidate_genes))
	
	# 2.3 align all core genes
	# load all genes
	all_genes = {}
	for ffn_file in glob.glob(prodigal_dir+'/*.ffn'):
		for gene in SeqIO.parse(ffn_file, 'fasta'):
			all_genes[gene.name] = gene.seq
	# make all ffn files
	gene_ffn_dir = options.outdir+'/gene_ffn/'
	if not os.path.exists(gene_ffn_dir): os.mkdir(gene_ffn_dir)
	for line in open(pangenome_file, 'r'):
		if line[0] == '#': continue
		cols = line[:-1].split('\t')
		panID = cols[0]
		if panID not in candidate_genes: continue
		ffn_file = gene_ffn_dir+panID+'.ffn'
		if os.path.exists(ffn_file): continue
		ffn_fh = open(ffn_file, 'w')
		for gene in cols[1:]:
			if gene == '-': continue
			genome = gene.split('|')[0]
			ffn_fh.write('>%s\n%s\n' % (genome, all_genes[gene]))
		ffn_fh.close()
	
	# align all genes
	muscle_cmds = []
	muscle_dir = options.outdir+'/muscle_aln/'
	if not os.path.exists(muscle_dir): os.mkdir(muscle_dir)
	for gene_ffn in glob.glob(gene_ffn_dir+'*.ffn'):
		panID = os.path.basename(gene_ffn).split('.')[0]
		muscle_out = muscle_dir + panID +'.aln'
		if os.path.exists(muscle_out): continue
		muscle_cmds.append([algos[3], gene_ffn, muscle_out])
	pool = mp.Pool(options.num_proc)
	for cmd in muscle_cmds: pool.apply_async(run_muscle, args = cmd)
	pool.close()
	pool.join()
	sys.stdout.write('All muscle alignments are done!\n')
	
	# make trees			
	gene_tree_cmds = []
	newick_dir = options.outdir+'/newick/'
	if not os.path.exists(newick_dir): os.mkdir(newick_dir)
	for aln_file in glob.glob(muscle_dir+'/*.aln'):
		panID = os.path.basename(aln_file).split('.')[0]
		nwk_file = newick_dir + panID + '.newick'
		if os.path.exists(nwk_file): continue
		gene_tree_cmds.append([algos[4], aln_file, nwk_file])
	pool = mp.Pool(options.num_proc)
	for cmd in gene_tree_cmds: pool.apply_async(make_gene_tree, args = cmd)
	pool.close()
	pool.join()
	sys.stdout.write('All gene tree constructions are done!\n')
	

	distTmpDir = options.outdir+'/distTmp/'
	seqbootTmpDir = options.outdir+'/seqbootTmp/'
	phybootTmpDir = options.outdir+'/phybootTmp/'
	if not os.path.exists(distTmpDir): os.mkdir(distTmpDir)
	if not os.path.exists(phybootTmpDir): os.mkdir(phybootTmpDir)
	if not os.path.exists(seqbootTmpDir): os.mkdir(seqbootTmpDir)
	puzzle_cmds_file=options.outdir+'/puzzle.cmds'
	pfh=open(puzzle_cmds_file,'w')
	pfh.write('k\nk\nk\nw\nc\n4\nm\nm\ny\n')
	pfh.close()
	
	# make dist files
	dist_cmds = []
	for aln_file in glob.glob(muscle_dir+'/*.aln'):
		panID = os.path.basename(aln_file).split('.')[0]
		# convert .aln to .phy file
		phy_file = muscle_dir+'/'+panID+'.phy'
		if not os.path.exists(phy_file):
			alignments = AlignIO.parse(open(aln_file, 'rU'), 'fasta')
			AlignIO.write(alignments, open(phy_file, 'w'), 'phylip')
			
	# get alphas
	sys.stdout.write('Start getting the alpha values...\n')
	alphaFile = options.outdir+'/alpha.txt'
	alpha_cmds = []
	logfile = options.outdir+'/tmp.log'
	if not os.path.exists(alphaFile) or open(alphaFile, 'r').readlines()[-1][:-1] != '//':
		alphafh = open(alphaFile, 'w')
		for phyFile in glob.glob(muscle_dir+'/*.phy'):
			gene_id = os.path.basename(phyFile).split('.')[0]
			alpha_cmd = '%s %s < %s > %s' % (algos[5], phyFile,  puzzle_cmds_file, logfile)
			os.system(alpha_cmd)
			distFile=phyFile+'.dist'
			puzzleFile=phyFile+'.puzzle'
			alpha=getAlpha(puzzleFile)
			alphafh.write(gene_id+'\t'+str(alpha)+'\n')
			try:os.remove(distFile)
			except:continue
			try:os.remove(puzzleFile)
			except:continue
		alphafh.write('//\n')
		alphafh.close()
		sys.stdout.write("Gamma distribution parameter alpha estimation done!\n")	
	# load the alpha values
	alphas={}
	for line in open(alphaFile, 'r'):
		if line[:-1] == '//': break
		col=line[:-1].split('\t')
		gene_id=col[0]
		alpha=col[1]
		alphas[gene_id]=float(alpha)
	sys.stdout.write("Gamma distribution parameter alphas loaded!\n")				
	
	# seqboot 100 bootstrap trees
	sys.stdout.write("Now bootstrapping!\n")
	for phyFile in glob.glob(muscle_dir+'/*.phy'):
		gene_id = os.path.basename(phyFile).split('.')[0]
		seqbootFile=seqbootTmpDir+gene_id+'.seqboot'
		if os.path.exists(seqbootFile): continue
		seqbootCmds=options.outdir+'/seqboot.cmds'
		sfh=open(seqbootCmds,'w')
		sfh.write(phyFile+'\ny\n17\n')
		sfh.close()
		seqbootOutfile=os.getcwd()+'/outfile'
		command=algos[6]+' < '+seqbootCmds+' > '+logfile
		os.system(command)
		shutil.move(seqbootOutfile, seqbootFile)
	sys.stdout.write("Bootstrapping done!\n")
	
	sys.stdout.write("Now splitting and merging bootstrapping results!\n")		
	for seqbootFile in glob.glob(seqbootTmpDir+'/*.seqboot'):
		gene_id = os.path.basename(seqbootFile).split('.')[0]
		try: alpha = alphas[gene_id]
		except: continue
		genDistFile=distTmpDir+gene_id+'.dist'
		if os.path.exists(genDistFile): continue
		numLines=fileLines(seqbootFile)
		perFileLines=numLines/100
		temp_genDistFile = distTmpDir+gene_id+'.dist.tmp'
		command='split -l '+str(perFileLines)+' '+seqbootFile+' '+seqbootFile+'.'
		os.system(command)
		with open(puzzle_cmds_file, 'w') as pfh:
			pfh.write('k\nk\nk\nw\nc\n4\na\n'+str(alpha)+'\nm\nm\ny\n')
		for subSeqbootFile in glob.glob(seqbootFile+'.*'):
			command=algos[5]+' '+subSeqbootFile+' < '+puzzle_cmds_file+' > '+logfile
			os.system(command)
			distFile=subSeqbootFile+'.dist'
			puzzleFile=subSeqbootFile+'.puzzle'
			command='cat '+distFile+' >> '+temp_genDistFile
			os.system(command)
			try:
				os.remove(distFile)
				os.remove(subSeqbootFile)
				os.remove(puzzleFile)
			except OSError:
				continue
		os.system('mv %s %s' % (temp_genDistFile, genDistFile))
	sys.stdout.write("Bootstrapping results splitting and merging done!\n")				
	
	## build N-J trees
	sys.stdout.write("Now building NJ trees...\n")
	for distFile in glob.glob(distTmpDir+'*.dist'):
		gene_id=os.path.basename(distFile).split('.')[0]
		phybootFile=phybootTmpDir+gene_id+'.phyboot'
		if os.path.exists(phybootFile): continue
		neighborCmds=options.outdir+'/neighbor.cmds'
		nfh=open(neighborCmds,'w')
		nfh.write(distFile+'\nm\n100\n17\ny\n')
		nfh.close()
		command=algos[7] + ' < '+neighborCmds+' > '+logfile
		os.system(command)
		neighborOutfile=os.getcwd()+'/outfile'
		neighborOuttree=os.getcwd()+'/outtree'
		os.remove(neighborOutfile)
		phybootFile=phybootTmpDir+gene_id+'.phyboot'
		shutil.move(neighborOuttree, phybootFile)
	sys.stdout.write("All distances calculation and tree building done!\n")
			
	## output LGTs
	outfile = options.outdir+'/LGTs.results.txt'
	ofh=open(outfile, 'w')
	ofh.write('#Clade_1\tClade_2\tPangenome_ID\tGenome_1\tGenome_2\tGene_1\tGene_2\tBootstrap_val\tab_val\n')
	
	for phybootFile in glob.glob(phybootTmpDir+'/*.phyboot'):
		gene_id = os.path.basename(phybootFile).split('.')[0]
		distFile = distTmpDir+gene_id+'.dist'
		trees=list(Phylo.parse(phybootFile, 'newick'))
		sorted_gene_clades = sorted(candidate_genes[gene_id].keys())
		for i, clade1 in enumerate(sorted_gene_clades):
			if len(candidate_genes[gene_id][clade1]) < 2: continue
			for j, clade2 in enumerate(sorted_gene_clades):
				if i >= j: continue
				if len(candidate_genes[gene_id][clade2]) < 2: continue
				gene_dict = {}
				genomes1 = [x.split('|')[0] for x in candidate_genes[gene_id][clade1]]
				genomes2 = [x.split('|')[0] for x in candidate_genes[gene_id][clade2]]
				for g in candidate_genes[gene_id][clade1]:
					genome = g.split('|')[0]
					gene_dict[genome] = g
				for g in candidate_genes[gene_id][clade2]:
					genome = g.split('|')[0]
					gene_dict[genome] = g
				
				clade1_combo = list(combinations(genomes1, 2))
				clade2_combo = list(combinations(genomes2, 2))
				# get all quartet lists
				allQuartetList = {}
				for (node1, node2) in clade1_combo:
					for (node3, node4) in clade2_combo:
						uniqkey = sorted([node1, node2, node3, node4])
						if tuple(uniqkey) in allQuartetList: continue
						allQuartetList[(node1, node2, node3, node4)] = 1
				for nodes in allQuartetList.keys():
					nodeA, nodeB, nodeC, nodeD = nodes
					topo1 = 0
					topo2 = 0
					topo3 = 0
					for tree in trees:
						try:
							distAB = tree.distance(nodeA, nodeB)
							distCD = tree.distance(nodeC, nodeD)
							distAC = tree.distance(nodeA, nodeC)
							distAD = tree.distance(nodeA, nodeD)
							distBC = tree.distance(nodeB, nodeC)
							distBD = tree.distance(nodeB, nodeD)
							cross_dists = [distAC, distAD, distBC, distBD]
							min_cross_dist = min(cross_dists)
							if distAB < min_cross_dist and distCD < min_cross_dist:
								topo1 += 1
							if distAB > min_cross_dist:
								if distAB > distAC or distAB > distBC: topo2 += 1
								elif distAB > distAD or distAB > distBD: topo3 += 1
							if distCD > min_cross_dist:
								if distCD > distAC or distCD > distAD: topo3 += 1
								elif distCD > distBC or distCD > distBD: topo2 += 1
						except:
							continue
					
					#print gene_id, clade1, clade2, nodes, topo1, topo2, topo3
					S = sum([topo1, topo2, topo3])
					if S < 50: continue
					r1 = float(topo2)/S
					r2 = float(topo3)/S
					
					## output format
					#print pangenome[gene_id]
					gene_1 = gene_dict[nodeA]
					gene_2 = gene_dict[nodeB]
					gene_3 = gene_dict[nodeC]
					gene_4 = gene_dict[nodeD]
					
					if r1 > 0.95:
						rt1 = '%.4f' % r1
						ofh.write(clade1+'\t'+clade2+'\t'+gene_id+'\t'+nodeA+'\t'+nodeC+'\t'+gene_1+'\t'+gene_3+'\t'+str(S)+'\t'+str(rt1)+'\n')
						ofh.write(clade1+'\t'+clade2+'\t'+gene_id+'\t'+nodeB+'\t'+nodeD+'\t'+gene_2+'\t'+gene_4+'\t'+str(S)+'\t'+str(rt1)+'\n')
					if r2 > 0.95:
						rt2 = '%.4f' % r2
						ofh.write(clade1+'\t'+clade2+'\t'+gene_id+'\t'+nodeA+'\t'+nodeD+'\t'+gene_1+'\t'+gene_4+'\t'+str(S)+'\t'+str(rt2)+'\n')
						ofh.write(clade1+'\t'+clade2+'\t'+gene_id+'\t'+nodeB+'\t'+nodeC+'\t'+gene_2+'\t'+gene_3+'\t'+str(S)+'\t'+str(rt2)+'\n')
	
	ofh.close()
	sys.stdout.write('All successfully completed, bye!\n')
	return 0
		
if __name__ == '__main__': main()
